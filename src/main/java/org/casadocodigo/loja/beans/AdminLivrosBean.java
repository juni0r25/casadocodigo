package org.casadocodigo.loja.beans;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.casadocodigo.loja.daos.AutorDao;
import org.casadocodigo.loja.daos.LivroDao;
import org.casadocodigo.loja.models.Autor;
import org.casadocodigo.loja.models.Livro;

@Named
@RequestScoped
public class AdminLivrosBean {

	@Inject
	private LivroDao livroDao;
	@Inject
	private AutorDao autorDao;

	private Livro livro = new Livro();
	private List<Integer> autoresId = new ArrayList<>();

	@Transactional
	public void salvar() {
		
		for(Integer id : autoresId) {
			livro.getAutores().add(new Autor(id));
		}
		
		livroDao.salvar(livro);
		System.out.println("Livro cadastrado : " + this.livro);
		this.limpar();
	}
	
	private void limpar() {
		this.livro = new Livro();
		this.autoresId = new ArrayList<Integer>();
	}

	public List<Autor> getAutores() {
		return autorDao.listar();
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public List<Integer> getAutoresId() {
		return autoresId;
	}

	public void setAutoresId(List<Integer> autoresId) {
		this.autoresId = autoresId;
	}
	
}
